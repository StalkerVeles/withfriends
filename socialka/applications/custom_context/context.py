
from django.utils.translation import ugettext_lazy as _

from conf import ACCOUNTS
from conf import STATIC_URL
from socialka.settings import SITE_NAME


def main(request, context={}):
    context['STATIC_URL']=STATIC_URL
    context['request']=request
    context['logo_title']=SITE_NAME

    if ACCOUNTS:
        try:
            context['account']=ACCOUNTS.objects.get(user=request.user)
        except:
            context['account']='anonimus'
    return context