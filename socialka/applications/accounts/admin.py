from django.contrib import admin

# Register your models here.
from applications.accounts.models import Accounts


@admin.register(Accounts)
class AccountsAdmin(admin.ModelAdmin):
    list_display = (
        'user',
        #'alias',
        #'content'
    )
    list_display_links = ['user',]