#coding:utf8
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render
from django.utils.translation import ugettext as _
# Create your views here.
from applications.accounts.models import Accounts


def my_page(request,pk,template='accounts/my_page.html',context={}):
    context['object']=Accounts.objects.get(id=pk)
    response=render(request,template,context)
    return response

def change_account(request):
    q=Accounts.objects.get(user=request.user)
    p=request.POST.dict()
    if p['username'] or p['email'] != '':
        q.user.username=p['username']
        q.user.email=p['email']
        q.user.first_name = p['first_name']
        q.user.last_name = p['last_name']
        q.user.save()
        return JsonResponse({'message':_(u'data saved successfully'),'status':'success'})
    return JsonResponse({'message':_('username or email not valid'),'status':'error'})