from __future__ import unicode_literals


from django.contrib.auth import logout as out
from django.contrib.auth.models import User
from django.db import models

# Create your models here.
from django.db.transaction import TransactionManagementError
from django.db.utils import IntegrityError
from django.dispatch import receiver
from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.db.models.signals import post_save

class Accounts(models.Model):
    user=models.OneToOneField(User)
    def __unicode__(self):
        return '%s'%self.user.username

@receiver(post_save, sender=User)
def create_user_accounts(sender, instance, created, **kwargs):
    if created:
        Accounts.objects.create(user=instance)



def logout(request):
    out(request)
    return redirect(reverse_lazy('base'))

def sign_up(request):
    post=request.POST.dict()
    if post['username'] or post['password'] or post['email'] !='':
        u=User(username=post['username'],password=post['password'],email=post['email'])
        u.save()
        Accounts(user=u).save()

    return redirect(reverse_lazy('base'))