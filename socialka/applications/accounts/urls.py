from registration.views import RegistrationView
from django.conf.urls import url
from registration.forms import RegistrationFormUniqueEmail

from applications.accounts.models import logout, sign_up
from django.contrib.auth import views as auth_views

from applications.accounts.views import my_page,change_account

urlpatterns=[
    url(
        r'^register/$',
        RegistrationView.as_view(),
        {'form': RegistrationFormUniqueEmail}, name='registration_register'),
    #url(r'^login/$', auth_views.login, name='login'),
    #url(r'^logout/$', logout, name='logout'),
    #url(r'^sign-up/$',sign_up,name='sign-up'),
    #url(r'^change_account/$',change_account,name='change_account')

]
#urlpatterns.append(url(r'^(?P<pk>[^/]+)/$',my_page,name='my_page'))